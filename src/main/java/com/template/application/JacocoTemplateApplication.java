package com.template.application;

import com.template.service.CalculatorService;

public class JacocoTemplateApplication {

    private static CalculatorService calculatorService = new CalculatorService();

    public static void main(String[] args) {
        System.out.println(calculatorService.add(5, 3));
    }
}
