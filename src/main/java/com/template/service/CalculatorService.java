package com.template.service;

public class CalculatorService {

    public double add(double number1, double number2) {
        return number1 + number2;
    }

    public double substract(double number1, double number2) {
        return number1 - number2;
    }

    public double multiply(double number1, double number2) {
        return number1 * number2;
    }

    public double divide(double number1, double number2) {
        return number1 / number2;
    }

    public double checkIfNumberIsEven(double number) {
        if (number % 2 == 0) {
            return Math.pow(number, 2);
        } else {
            return 3 * number;
        }
    }
}
