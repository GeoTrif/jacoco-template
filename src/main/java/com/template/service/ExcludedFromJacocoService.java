package com.template.service;

public class ExcludedFromJacocoService {

    public boolean checkIfOddNumber(int number) {
        System.out.println("Excluded from test");

        return number % 2 != 0;
    }
}
