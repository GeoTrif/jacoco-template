package com.template.service;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CalculatorServiceTest {

    @Test
    public void add() {
        // Given
        CalculatorService calculatorService = new CalculatorService();
        double number1 = 5;
        double number2 = 3;

        // When
        double actualValue = calculatorService.add(number1, number2);

        // Then
        assertEquals(8.0, actualValue, 0);
    }

    @Test
    public void substract() {
        // Given
        CalculatorService calculatorService = new CalculatorService();
        double number1 = 5;
        double number2 = 3;

        // When
        double actualValue = calculatorService.substract(number1, number2);

        // Then
        assertEquals(2.0, actualValue, 0);
    }

    @Test
    public void multiply() {
        // Given
        CalculatorService calculatorService = new CalculatorService();
        double number1 = 5;
        double number2 = 3;

        // When
        double actualValue = calculatorService.multiply(number1, number2);

        // Then
        assertEquals(15.0, actualValue, 0);
    }

    @Test
    public void divide() {
        // Given
        CalculatorService calculatorService = new CalculatorService();
        double number1 = 6;
        double number2 = 3;

        // When
        double actualValue = calculatorService.divide(number1, number2);

        // Then
        assertEquals(2.0, actualValue, 0);
    }

    @Test
    public void checkIfNumberIsEven_evenNumber() {
        // Given
        CalculatorService calculatorService = new CalculatorService();
        int number = 4;

        // When
        double actualValue = calculatorService.checkIfNumberIsEven(number);

        assertEquals(16, actualValue, 0);
    }

    @Test
    public void checkIfNumberIsEven_oddNumber() {
        // Given
        CalculatorService calculatorService = new CalculatorService();
        int number = 5;

        // When
        double actualValue = calculatorService.checkIfNumberIsEven(number);

        assertEquals(15, actualValue, 0);
    }
}
